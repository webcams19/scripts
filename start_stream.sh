#!/bin/bash

# Check for argument
if [ -z $1 ]
then
    printf "\033[1;31mError: Missing target IP.\033[0m\nUsage:\n$0 <ip_address>\n"
    exit
fi

# Print some information for the user
echo "Stream starting.  To view, go to the target computer ($1) and run the command:"
printf " \033[1;33mvlc udp://@$1:1234\033[0m\n"

# Start the stream to the target IP address
raspivid -t 0 -b 5000000 -md 1 -fps 24 -o - | ffmpeg -loglevel panic -re -i - -vcodec copy -f mpegts udp://$1:1234

# Done!
printf "\033[1;31mStream has ended !\033[0m\n"
