#!/bin/bash

# It's not what it looks like, I swear!

# Kills a processes children, and then kills the process.  Pretty frickin'
# metal if you ask me.
#
# Nicholas Hollander <nhhollander@wpi.edu>
# 2019-06-24

# Find child processes
children=$(pgrep -P $1)
# Kill the children and parent
kill -9 $children $1
