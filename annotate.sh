#!/bin/bash

# Check for arguments
if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ]; then
    printf "\033[1;31mError: Missing arguments\033[0m\n"
    echo "Usage:"
    echo "$0 <filename> <timestamp>"
    exit
fi

# Annotates the crap out of images
convert "$1" \
	-resize 1920x1080^ -gravity center -extent 1920x1080 \
	-bordercolor black -border 0x25 -fill white -pointsize 20 -font Arial \
	-gravity southeast -annotate +5+0 "$3" \
	-gravity northwest -annotate +5+0 "$4" \
	-gravity northeast -annotate +5+0 'Worcester Polytechnic Institute' \
	-gravity southwest -annotate +5+0 "$2" \
	-quality 80 "$1"


