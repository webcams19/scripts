# Remote port opener script

This is a silly script I made to allow users to remotely SSH into this machine
from the outside of a NAT.

Written by Nicholas Hollander <nhhollander@wpi.edu>

## Setup

There are several prerequisites to get this script working, listed below.

1.  Configured SSH certificate.
    This requires that your `~/.ssh.config` file is set upt o automatically
    authenticate to `nps-cams.wpi.edu`
2.  Set up `crontab`.
    Edit the user crontab to include the following line (modified dpending on
    where you installed the script file)
    
        @reboot /home/pi/scripts/open_remote_port.sh

## Usage

Once the script is set up, it will automatically open and preserve a connection
to nps-cams.wpi.edu, where it will open a remote port back to this machine.

To connect, from any computer run `ssh pi@nps-cams.wpi.edu -p ####` where the
port number matches the value configured in the script file.
