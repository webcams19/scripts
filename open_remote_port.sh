#!/bin/bash

# This script forwards a port from nps-cams.wpi.edu to this machine so that it
# can be remotely administered despite the fact that it does not have a public
# IP address.
#
# Nicholas Hollander 2019-06-19

# Daemonize the script
(

# Remote port
remote_port=2210

# Timestamp command
ts_cmd=`printf 'date +%%Y-%%m-%%d\u00A0%%H-%%M-%%S'`

# Print the pid so we can kill the script if (when) it goes off the rails
echo "[$($ts_cmd)] == restarted =="
echo "[$($ts_cmd)] $0 is running with pid $BASHPID"
echo "[$($ts_cmd)] Kill with '~/scripts/kill_children.sh $BASHPID'"

# Wait a little bit for the network to come online
echo "[$($ts_cmd)] Waiting 10 seconds before startup."
sleep 10

# Temination handlers
function handle_hup() {
    echo "[$($ts_cmd)] == Hung up (1) =="; exit
}
function handle_int() {
    echo "[$($ts_cmd)] == Interrupted (2) =="; exit
}
function handle_quit() {
    echo "[$($ts_cmd)] == Quitting (3) =="; exit
}
function handle_term() {
    echo "[$($ts_cmd)] == Terminated (15) =="; exit
}
# Register terminatio handlers
trap handle_hup HUP
trap handle_int INT
trap handle_quit QUIT
trap handle_term TERM 

# Create an eternal loop.  We want to keep retrying the connection until it
# works.
while :
do

    # Print a message to keep the logs in the loop 
    echo "[$($ts_cmd)] Opening new connection..."

    # Attempt to establish the remote connection
    # ServerAliveInterval 20 and ServerAliveCountMax 3 mean that the script
    # will send a null packet every 20 seconds, and assume the connection is
    # dead if the server fails to respond 3 times in a row.
    ssh -N -T -4 -o ServerAliveInterval=20 -o ServerAliveCountMax=3 -R $remote_port:*:22 nps-cams.wpi.edu

    # At this point in the script, the connection is dead!  Oh no!
    # We'll wait a little bit and then try again
    echo "[$($ts_cmd)] Connection lost - resetting in 60 seconds..."
    sleep 60

done

) < /dev/null >> /tmp/open_remote_port.log 2>&1 &

# Banish children to the shadow realm
disown
