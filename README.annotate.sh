# Automatic Image Annotation Script

This script automatically annotates an image with various details, overwriting
the original image in the process.  The script takes two arguments, the first
specifying the name of the file to be modified, and the second specifying the
timestamp to add to the lower left corner.  The other three values, placed in
the upper left, upper right, and lower right corners of the images, are hard
coded in this version of the script.
